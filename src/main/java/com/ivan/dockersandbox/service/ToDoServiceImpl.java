package com.ivan.dockersandbox.service;

import com.ivan.dockersandbox.model.ToDoEntry;
import com.ivan.dockersandbox.repository.ToDoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToDoServiceImpl implements ToDoService {

  private final ToDoRepository repository;

  public ToDoServiceImpl(ToDoRepository repository) {
    this.repository = repository;
  }

  @Override
  public List<ToDoEntry> findAll() {
    return repository.findAll();
  }

  @Override
  public ToDoEntry create(ToDoEntry entry) {
    return repository.save(entry);
  }
}
