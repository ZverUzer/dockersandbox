package com.ivan.dockersandbox.service;

import com.ivan.dockersandbox.model.ToDoEntry;

import java.util.List;

public interface ToDoService {
    List<ToDoEntry> findAll();

    ToDoEntry create(ToDoEntry entry);
}
