package com.ivan.dockersandbox.web.rest;

import com.ivan.dockersandbox.model.ToDoEntry;
import com.ivan.dockersandbox.service.ToDoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/v1/todos")
@Slf4j
public class ToDoResource {

  private final ToDoService toDoService;

  public ToDoResource(ToDoService toDoService) {
    this.toDoService = toDoService;
  }

  @GetMapping
  public ResponseEntity<List<ToDoEntry>> getAllToDos() {
    log.info("REST request to get all DoDos");
    return ResponseEntity.ok(toDoService.findAll());
  }

  @PostMapping
  public ResponseEntity<ToDoEntry> create(@RequestBody ToDoEntry entry) {
    log.info(format("REST request to create ToDo: {%s}", entry.toString()));

    ToDoEntry created = toDoService.create(entry);

    return ResponseEntity.status(CREATED).body(created);
  }
}
