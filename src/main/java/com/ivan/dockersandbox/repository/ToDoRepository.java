package com.ivan.dockersandbox.repository;

import com.ivan.dockersandbox.model.ToDoEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToDoRepository extends JpaRepository<ToDoEntry, Long> {
}
