package com.ivan.dockersandbox.model;

import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.AUTO;

@Entity
@ToString
public class ToDoEntry {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    private String text;
    private boolean completed;

    public Long getId() {
        return id;
    }

    public ToDoEntry setId(Long id) {
        this.id = id;
        return this;
    }

    public String getText() {
        return text;
    }

    public ToDoEntry setText(String text) {
        this.text = text;
        return this;
    }

    public boolean isCompleted() {
        return completed;
    }

    public ToDoEntry setCompleted(boolean completed) {
        this.completed = completed;
        return this;
    }
}
