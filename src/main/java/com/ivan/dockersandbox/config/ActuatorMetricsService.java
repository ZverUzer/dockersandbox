package com.ivan.dockersandbox.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.actuate.info.InfoEndpoint;
import org.springframework.boot.actuate.metrics.MetricsEndpoint;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import static java.lang.String.format;
import static java.util.Collections.emptyList;

@Service
@Slf4j
public class ActuatorMetricsService {
  private final MetricsEndpoint metricsEndpoint;
  private final HealthEndpoint healthEndpoint;
  private final InfoEndpoint infoEndpoint;

  public ActuatorMetricsService(
      MetricsEndpoint metricsEndpoint, HealthEndpoint healthEndpoint, InfoEndpoint infoEndpoint) {
    this.metricsEndpoint = metricsEndpoint;
    this.healthEndpoint = healthEndpoint;
    this.infoEndpoint = infoEndpoint;
  }

  @Scheduled(initialDelay = 6000, fixedDelay = 60000)
  public void fetchMetrics() {
    metricsEndpoint
        .listNames()
        .getNames()
        .forEach(
            name ->
                log.info(
                    format(
                        "%s: %s",
                        name, metricsEndpoint.metric(name, emptyList()).getMeasurements())));
  }

  @Scheduled(initialDelay = 6000, fixedDelay = 30000)
  public void fetchHealth() {
    Health health = healthEndpoint.health();
    log.info(format("Health: %s", health.getStatus()));
  }

  @Scheduled(initialDelay = 6000, fixedDelay = 60000)
  public void fetchInfo() {
    infoEndpoint.info().forEach((k, v) -> log.info(format("%s: %s", k, v)));
  }
}
