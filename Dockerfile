FROM gradle:6.6.1-jdk11 AS build
COPY --chown=gradle:gradle . /app/
WORKDIR /app
RUN ./gradlew build --stacktrace

FROM openjdk:11
COPY --from=build /app/build/libs/*.jar app.jar
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","/app.jar"]