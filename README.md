# README #

Sandbox project to play with Docker

### Swagger ###
##### Running from IDE or Docker Compose #####
* http://localhost:8080/v2/api-docs
* http://localhost:8080/swagger-ui.html
##### Running from Docker #####
* http://localhost:3000/v2/api-docs
* http://localhost:3000/swagger-ui.html

### Set up ###

#### Start from compose ####
* docker-compose up -d

#### Start each service separately ####
##### Build .jar #####
* ./gradlew build
##### Build Docker image #####
* docker build -t docker-sandbox .
##### Run Docker container in DB network #####
* docker run -dp 3000:8080 --network docker-sandbox docker-sandbox

##### Set up database #####
* docker network create docker-sandbox
* docker run -d \
      --network docker-sandbox --network-alias mysql \
      -v docker-sandbox-mysql-data:/var/lib/mysql \
      -e MYSQL_ROOT_PASSWORD=secret \
      -e MYSQL_DATABASE=todos \
      mysql:5.7

##### Start local DB #####
* docker run -dp 3306:3306 -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_ROOT_HOST=% -e MYSQL_DATABASE=todos mysql/mysql-server --init-connect='GRANT CREATE USER ON *.* TO 'root'@'%';FLUSH PRIVILEGES;'      

##### Troubleshooting #####
* docker run -it --network docker-sandbox nicolaka/netshoot

### Contribution guidelines ###

* Writing tests
* Use Google Java format

##### Tag new images #####
* docker tag docker-sandbox zveruzer/docker-sandbox
##### Push image #####
* docker push zveruzer/docker-sandbox

### Who do I talk to? ###

* Repo owner or admin